CREATE OR REPLACE PROCEDURE p_wall_board_nacional IS
  CURSOR c_servicios IS
    SELECT s.nume_serv
          ,s.nume_base
          ,s.nume_esta_serv estado_del_servicio
          ,CASE
             WHEN nume_tipo_serv = 1 THEN
              'Mec�nica'
             WHEN nume_tipo_serv = 2 THEN
              'Traslados'
              ELSE
                'Otros'
           END tipo_servicio
          ,CASE
             WHEN nume_tipo_serv = 1 THEN
              1
           END mecanica
          ,CASE
             WHEN nume_tipo_serv = 2 THEN
              1
           END traslados
          ,CASE
             WHEN nume_cier = 100 AND nume_esta_serv >= 60 OR (nume_cier IN (6, 113, 15, 11, 4, 5, 12, 17, 3, 14, 13, 24, 10, 26, 111, 2, 28, 18)) THEN
              '1'
           END finalizados
          ,CASE
             WHEN nume_cier IN (6, 113, 15, 11, 4, 5, 12, 17, 3, 14, 13, 24, 10, 26, 111, 2, 28) THEN
              1
           END anulados
          ,CASE
             WHEN flag_recl > 0 THEN
              1
             ELSE
              0
           END servicios_reclamados
      FROM servicios s
     WHERE nume_serv >= to_char(trunc(SYSDATE), 'yyyymmdd') || 100000
       AND (nume_base NOT IN (6001, 10541, 8700, 5052) OR nume_base IS NULL)
       AND nume_esta_serv > 0; --trafico
  --OR nume_base IS NULL;

  --     AND nume_tipo_serv IN (2, 1)
  --AND nume_cier NOT IN (6, 113, 15, 11, 4, 5, 12, 17, 3, 14, 13, 18, 24, 10, 26, 111, 2, 28);
  v_demora_real          t_wall_board_nacional.demora_real%TYPE;
  v_demoras              t_wall_board_nacional.demoras%TYPE;
  v_cantidad_finalizados t_wall_board_nacional.cantidad_finalizados%TYPE;
  v_count                NUMBER(4);
  v_error                VARCHAR2(500);
  v_demora_real_60_min   t_wall_board_nacional.demora_60_min%TYPE;
  v_cant                 NUMBER(2);
  v_esperando_aceptacion t_wall_board_nacional.esperando_aceptacion%TYPE;
  v_demora_aceptacion    t_wall_board_nacional.demora_aceptacion%TYPE;
  v_demora_derivacion    t_wall_board_nacional.demora_derivacion%TYPE;
  v_derivadores          NUMBER(3);
BEGIN
  DELETE FROM t_wall_board_nacional
   WHERE nro_servicio IS NOT NULL;
  FOR r IN c_servicios LOOP
    SAVEPOINT p_save;
    BEGIN
      v_cant                 := NULL;
      v_demora_real          := NULL;
      v_demoras              := NULL;
      v_demora_real_60_min   := NULL;
      v_cantidad_finalizados := NULL;
      v_error                := NULL;
      v_count                := NULL;
      v_demora_aceptacion    := NULL;
      v_demora_derivacion    := NULL;
      v_esperando_aceptacion := NULL;
      IF r.anulados IS NULL THEN
        SELECT COUNT(1)
          INTO v_count
          FROM facts_crm
         WHERE numero_servicio = r.nume_serv;
      
        SELECT COUNT(1)
          INTO v_demoras
          FROM servicioshorarios
         WHERE nume_serv = r.nume_serv
           AND hora_pedi >= ((SYSDATE - (180 / 24) / 60))
           AND hora_lleg IS NULL
           AND hora_prog IS NULL;
      
        BEGIN
          SELECT nvl(trunc((hora_lleg - hora_pedi) * 24 * (60)), 0) demora_real
            INTO v_demora_real
            FROM servicioshorarios
           WHERE nume_serv = r.nume_serv
             AND hora_fina >= ((SYSDATE - (60 / 24) / 60))
             AND hora_lleg IS NOT NULL
             AND hora_prog IS NULL;
        EXCEPTION
          WHEN OTHERS THEN
            v_error := SQLERRM || ' error en calcular demora real ' || r.nume_serv;
            NULL;
        END;
      
        BEGIN
          SELECT COUNT(1)
            INTO v_esperando_aceptacion
            FROM servicioshorarios
           WHERE hora_deri IS NULL
             AND hora_acep IS NULL
             AND hora_prog IS NULL
             AND hora_fina IS NULL
             AND hora_lleg IS NULL
             AND nume_serv = r.nume_serv;
        EXCEPTION
          WHEN OTHERS THEN
            v_error := SQLERRM || ' error en calcular servicios esperando aceptacion ' || r.nume_serv;
        END;
      
        BEGIN
          SELECT nvl(trunc((hora_deri - hora_pedi) * 24 * (60)), 0)
            INTO v_demora_derivacion
            FROM servicioshorarios
           WHERE hora_deri IS NOT NULL
             AND nvl(trunc((hora_deri - hora_pedi) * 24 * (60)), 0) > 1
             AND hora_deri >= ((SYSDATE - (60 / 24) / 60))
             AND nume_serv = r.nume_serv;
        EXCEPTION
          WHEN OTHERS THEN
            v_error             := SQLERRM || ' error en calcular demora derivacion ' || r.nume_serv;
            --v_demora_derivacion := NULL;
        END;
      
        BEGIN
          SELECT nvl(trunc((hora_acep - hora_pedi) * 24 * (60)), 0)
            INTO v_demora_aceptacion
            FROM servicioshorarios
           WHERE hora_acep IS NOT NULL
             AND nvl(trunc((hora_deri - hora_pedi) * 24 * (60)), 0) > 1
             AND hora_acep >= ((SYSDATE - (60 / 24) / 60))
             AND nume_serv = r.nume_serv;
        EXCEPTION
          WHEN OTHERS THEN
            v_error             := SQLERRM || ' error en calcular demora aceptacion ' || r.nume_serv;
          --  v_demora_aceptacion := NULL;
        END;
      
        BEGIN
          SELECT COUNT(1)
            INTO v_cant
            FROM servicioshorarios
           WHERE nume_serv = r.nume_serv
             AND hora_lleg >= ((SYSDATE - (90 / 24) / 60))
             AND hora_lleg IS NOT NULL
             AND hora_prog IS NULL;
          IF v_cant = 1 THEN
            SELECT nvl(trunc((hora_lleg - hora_pedi) * 24 * (60)), 0) demora_real
              INTO v_demora_real_60_min
              FROM servicioshorarios
             WHERE nume_serv = r.nume_serv
               AND hora_lleg >= ((SYSDATE - (90 / 24) / 60))
               AND hora_lleg IS NOT NULL
               AND hora_prog IS NULL;
          END IF;
        EXCEPTION
          WHEN no_data_found THEN
            v_demora_real_60_min := NULL;
            GOTO finalizar;
            ROLLBACK TO p_save;
        END;
      END IF;
    EXCEPTION
      WHEN no_data_found THEN
        v_error := 'Error al buscar los horarios para el servicio ' || r.nume_serv;
        GOTO finalizar;
        ROLLBACK TO p_save;
      WHEN OTHERS THEN
        v_error := SQLERRM;
        GOTO finalizar;
        ROLLBACK TO p_save;
    END;
    SELECT COUNT(1)
      INTO v_derivadores
      FROM vw_derivadores
     WHERE derivador_activo = 1;
    <<finalizar>>
    BEGIN
      INSERT INTO dba_sosdw.t_wall_board_nacional
        (nro_servicio
        ,tipo_servicio
        ,cantidad_mecanica
        ,cantidad_traslados
        ,demora_60_min
        ,demora_real
        ,cantidad_finalizados
        ,cantidad_anulados
        ,demoras -- demoras mas de 3 horas que aun no han cerrado
        ,demora_aceptacion
        ,demora_derivacion
        ,esperando_aceptacion
        ,servicios_derivador
        ,derivadores_disponibles)
      VALUES
        (r.nume_serv
        ,r.tipo_servicio
        ,r.mecanica
        ,r.traslados
        ,CASE WHEN v_demora_real_60_min > 0 THEN v_demora_real_60_min END
        ,CASE WHEN v_demora_real > 0 THEN v_demora_real END
        ,r.finalizados
        ,r.anulados --servicio anulado
        ,v_demoras -- servicios abiertos de las ultimas 3 horas
        ,CASE WHEN v_demora_aceptacion > 0 THEN v_demora_aceptacion END
        ,CASE WHEN v_demora_derivacion > 0 THEN v_demora_derivacion END
        ,v_esperando_aceptacion --CASE WHEN v_demora_aceptacion IS NULL AND v_demora_derivacion IS NULL THEN 1 END
        ,CASE WHEN v_demora_derivacion IS NOT NULL THEN 1 END
        ,v_derivadores);
    EXCEPTION
      WHEN OTHERS THEN
        v_error := 'Error al insertar los valores para el registro ' || r.nume_serv || ' ' || SQLERRM;
        ROLLBACK TO p_save;
    END;
    COMMIT;
  END LOOP;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    v_error := 'Error al ejecutar p_wall_board ' || SQLERRM;
END p_wall_board_nacional;
/
